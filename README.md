# Web Application for Custom Object Realtion Mapping tool

## Project Description

A custom object relational mapping (ORM) framework, written in Java, which allows for a simplified and SQL-free interaction with a relational data source. Low-level JDBC is completely abstracted away from the developer, allowing them to easily query and persist data to a data source. Makes heavy use of the Java Reflection API in order to support CRUD operations for any entity objects as defined by the developer. A servlet-based web application was used to demonstrate the functionality of the ORM.

## Technologies Used

* Java
* JDBC
* Maven
* GitLab
* JUnit
* Mockito
* PostgreSQL
* Postman
* AWS RDS
* DBBeaver
* Elastic Beanstalk
* Tomcat

## Features

List of features ready
* User is able to add game to database.
* User can retrive any game details from database based ontheir name.
* User can remove any game from database.

## Getting Started
   
* git clone https://gitlab.com/210614-java-react-enterprise/project-1/team-4-webapp.git
* mvn clean package

## Contributors

* Apurv Patel
* Curtis Morgan
* Melanie Duah 

