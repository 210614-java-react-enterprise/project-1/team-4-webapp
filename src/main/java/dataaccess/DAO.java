package dataaccess;

import java.util.*;

public interface DAO<T> {
    /**
     * Takes a model and insert a record into to the table indicated in the Class's Table annotation
     * @param model
     * @throws DAOException
     */
    void create(T model) throws DAOException;

    void update(T model) throws DAOException;

    void delete(T model) throws DAOException;

    T find(Object primaryKey) throws DAOException;

    List<T> findAll() throws DAOException, DataSourceException;

    List<T> findWhere(Map<String, Object> properties) throws DAOException;
}
