package dataaccess;

import dataaccess.annotations.*;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * Contains utility methods for various reflection tasks including reading class annotations, field and their annotations etc.
 * and populating SQL prepared statement parameters with values read with reflection from models
 */
public class ReflectionHelper {

    public static String getTableName(Class<?> tableClass) {
        String tableName;

        if (!tableClass.isAnnotationPresent(Table.class))
            throw new IllegalStateException("Given model class is not an Table");

        tableName = tableClass.getAnnotation(Table.class).name();
        if (tableName.equals(""))
            tableName = tableClass.getSimpleName();

        return tableName;
    }

    /**
     * Takes a model class and iterate through it's annotated fields to generate the column names and placeholder (?) values
     * to be used in an insert statement
     * Sample output:
     * columns : id, name, email
     * valuePlaceHolders ?, ?, ?
     * @return columnSql
     */
    public static ColumnSql getInsertColumnsSql(Class<?> tableClass) {
        ColumnSql columnSql = new ColumnSql();

        Field[] fields = tableClass.getDeclaredFields();
        StringBuilder columns = new StringBuilder();
        StringBuilder values = new StringBuilder();

        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if (shouldIncludeFieldInSQL(field)) {
                columns.append(getColumnName(field));
                values.append("?");
                if (i + 1 < fields.length) {
                    columns.append(",");
                    values.append(",");
                }
            }
        }
        columnSql.setColumnNames(columns.toString());
        columnSql.setValuePlaceHolders(values.toString());

        return columnSql;
    }


    /**
     * Takes a model class and iterate through it's annotated fields to generate the column names and placeholder (?) values
     * to be used in an update statement
     * Sample output:
     * columns : id = ?, name = ?, email = ?
     * @return columnSql
     */
    public static ColumnSql getUpdateColumnSql(Class<?> tableClass) {
        ColumnSql columnSql = new ColumnSql();
        Field[] fields = tableClass.getDeclaredFields();
        StringBuilder columns = new StringBuilder();

        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];

            if (shouldIncludeFieldInSQL(field)) {
                columns.append(getColumnName(field));
                columns.append("=");
                columns.append("?");
                if (i + 1 < fields.length) {
                    columns.append(",");
                }
            }
        }
        String idFieldName = getIdFieldName(tableClass);
        String where = idFieldName + " = ?";
        columnSql.setWhereConditions(where);
        columnSql.setColumnNames(columns.toString());

        return columnSql;
    }


    /**
     * Gets the Field on the given class annotated with @Id
     * @return idField
     */

    public static Field getIdField(Class<?> tableClass) {
        Field[] fields = tableClass.getDeclaredFields();
        Field idField = Arrays.stream(fields)
                .filter(f -> f.isAnnotationPresent(Id.class))
                .findFirst().orElse(null);

        return idField;
    }

    /**
     * Gets the name of the Field on the given class annotated with @Id
     * @return getColumnName
     */
    public static String getIdFieldName(Class<?> tableClass) {
        Field idField = getIdField(tableClass);
        return getColumnName(idField);
    }

    /**
     * Takes a model class and creates a SQL where condition based on the field annotated with @Id
     * Sample output:
     * whereCondition : id = ?
     *
     * @return columnSql
     */
    public static ColumnSql getWhereIdCondition(Class<?> tableClass) {
        ColumnSql columnSql = new ColumnSql();
        String idFieldName = getIdFieldName(tableClass);
        String whereCondition = idFieldName + " = ?";
        columnSql.setWhereConditions(whereCondition);
        return columnSql;
    }

    /**
     * Takes a model class and iterates through its fields to create a SQL string with a where condition to be used in a
     * SELECT statement
     * Sample output:
     * columns: id, name, email
     * whereCondition : id = ?
     *
     * @return columnSql
     */
    public static ColumnSql getSelectColumns(Class<?>tableClass){
        ColumnSql columnSql = new ColumnSql();

        Field[] fields = tableClass.getDeclaredFields();
        StringBuilder columns = new StringBuilder();

        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if (isValidColumn(field)) {
                columns.append(getColumnName(field));
                if (i + 1 < fields.length) {
                    columns.append(",");
                }
            }
        }
        String idFieldName = getIdFieldName(tableClass);
        String whereCondition = idFieldName + " = ?";
        columnSql.setWhereConditions(whereCondition);
        columnSql.setColumnNames(columns.toString());
        return columnSql;
    }

    /**
     * Takes a model class and iterates through its fields to create a SQL string to be used in a SELECT statement
     * Sample output:
     * columns: id, name, email
     *
     * @return columnSql
     */
    public static ColumnSql getAllSelectColumns(Class<?>tableClass){
        ColumnSql columnSql = new ColumnSql();

        Field[] fields = tableClass.getDeclaredFields();
        StringBuilder columns = new StringBuilder();

        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if (isValidColumn(field)) {
                columns.append(getColumnName(field));
                if (i + 1 < fields.length) {
                    columns.append(",");
                }
            }
        }
        columnSql.setColumnNames(columns.toString());
        return columnSql;
    }
    /**
     *it is use to check if there is annotation generatevalue that says the id is auto increament.
     * @return isValidColumn && !field.isAnnotation
     */
    private static boolean shouldIncludeFieldInSQL(Field field) {
        return isValidColumn(field) && !field.isAnnotationPresent(GeneratedValue.class);

    }
    /**
     * use to check if there is annotation column and joinColumn class.
     */
    private static boolean isValidColumn(Field field) {
        return field.isAnnotationPresent(Column.class) ||
                field.isAnnotationPresent(JoinColumn.class);
    }

    /**
     * use to check if there is column and joinColumn class.
     * @return columnName
     */
    private static String getColumnName(Field field) {
        String columnName = "";
        if(field != null) {
            JoinColumn joinColumn = field.getAnnotation(JoinColumn.class);
            if (joinColumn != null) {
                columnName = joinColumn.name();
            } else {
                Column column = field.getAnnotation(Column.class);
                columnName = column.name();
            }
            if (columnName.equals(""))
                columnName = field.getName();
        }
        return columnName;
    }

    /**
     * use to check set preparedStatement.
     */
    public static <T> void setPreparedStatementParameters(T model, PreparedStatement preparedStatement)
            throws IllegalAccessException, SQLException {
        Field[] fields = model.getClass().getDeclaredFields();
        int statementParameterIndex = 1;

        for (Field field : fields) {
            Object dataObject = model;
            if (shouldIncludeFieldInSQL(field)) {
                if (field.isAnnotationPresent(JoinColumn.class)) {
                    Field referenceField = getReferenceField(field);
                    if (referenceField != null) {
                        field.setAccessible(true);
                        referenceField.setAccessible(true);

                        Object referenceObject = field.get(model);
                        dataObject = referenceObject;
                        field = referenceField;
                    }
                }
                setPreparedStatementParameterValue(preparedStatement, statementParameterIndex, field, dataObject);
                statementParameterIndex++;
            }
        }
    }

    /**
     * use to check if the joinColumn have referenceColumnName if not set it to its default.
     * @return findFieldWithColumnName
     */
    private static Field getReferenceField(Field sourceField){
        String referenceName = sourceField.getAnnotation(JoinColumn.class).referenceColumnName();
        return findFieldWithColumnName(sourceField.getType(), referenceName);
    }

    /**
     * use to check set preparedStatement with whereCondition.
     */
    public static <T> void setUpdatePreparedStatement(T model, PreparedStatement preparedStatement)
            throws SQLException, IllegalAccessException {
        setPreparedStatementParameters(model, preparedStatement);
        Field[] fields = model.getClass().getDeclaredFields();
        Field idField = getIdField(model.getClass());
        int whereConditionIndex = (int) Arrays.stream(fields)
                .filter(f -> isValidColumn(f))
                .count();
        setPreparedStatementParameterValue(preparedStatement, whereConditionIndex, idField, model);
    }

    /**
     * use to check  preparedStatement to get the fields id from the model.
     * @param preparedStatement
     */
    public static <T> void setIDPreparedStatement(T model, PreparedStatement preparedStatement)
            throws SQLException, IllegalAccessException {

        Field idField = getIdField(model.getClass());
        setPreparedStatementParameterValue(preparedStatement, 1, idField, model);
    }

    /**
     * use to check set data type for the preparedStatement and select which is preferable .
     */
    public static void setPreparedStatementParameterValue(PreparedStatement preparedStatement, int index, Field field,
                                                          Object dataObject)
            throws IllegalAccessException, SQLException {

        if(field != null) {
            field.setAccessible(true);
            String fieldType = field.getType().getSimpleName();
            switch (fieldType) {
                case "int":
                    preparedStatement.setInt(index, field.getInt(dataObject));
                    break;
                case "char":
                case "String":
                    preparedStatement.setString(index, field.get(dataObject).toString());
                    break;
                case "boolean":
                    preparedStatement.setBoolean(index, field.getBoolean(dataObject));
                    break;
                case "double":
                    preparedStatement.setDouble(index, field.getDouble(dataObject));
                    break;
                case "LocalDate":
                    preparedStatement.setObject(index, field.get(dataObject), Types.DATE);
                    break;
                default:
                    preparedStatement.setObject(index, field.get(dataObject));
            }
        }
    }

    /**
     * use to find the field with the column name.
     * @return hasColumnWithName
     */
    private static Field findFieldWithColumnName(Class<?> classObject, String ColumnName) {
        Field[] fields = classObject.getDeclaredFields();
        Field fieldWithColumnName = Arrays.stream(fields)
                .filter(f -> hasColumnAnnotationWithName(f, ColumnName))
                .findFirst().orElse(null);

        return fieldWithColumnName;
    }

    /**
     * use to check if the field has column Annotation with a name.
     * @return hasColumnWithName
     */
    private static boolean hasColumnAnnotationWithName(Field field, String columnName) {
        boolean hasColumnWithName = false;

        Column column = field.getAnnotation(Column.class);
        if (column != null) {
            hasColumnWithName = column.name().equals(columnName) || field.getName().equals(columnName);
        }
        return hasColumnWithName;
    }

    /**
     * @return model
     */
    public static <T> T createModelFromResult(ResultSet results, Class<T> modelClass)
            throws SQLException, NoSuchMethodException, InvocationTargetException,
            InstantiationException, IllegalAccessException {

        T model = modelClass.getDeclaredConstructor().newInstance();
        Field[] fields = model.getClass().getDeclaredFields();
        int parameterIndex = 1;
        for(Field field : fields){
            if(isValidColumn(field)) {
                field.setAccessible(true);
                if(field.isAnnotationPresent(JoinColumn.class)){
                    Object referenceFieldObject = field.getType().getDeclaredConstructor().newInstance();
                    Field referenceField = getReferenceField(field);
                    referenceField.setAccessible(true);
                    referenceField.set(referenceFieldObject,convertToFieldType(results.getObject(parameterIndex),
                            referenceField));
                    field.set(model,referenceFieldObject);
                }else {
                    field.set(model, convertToFieldType(results.getObject(parameterIndex), field));
                }
                parameterIndex++;
            }
        }
        return model;
    }

    private static Object convertToFieldType(Object obj, Field field){
        Object result = obj;
        if(field.getType().getSimpleName().equals("LocalDate")){
            result = ((Date)obj).toLocalDate();
        }

        return  result;
    }
}
