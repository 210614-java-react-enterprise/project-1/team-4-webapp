package dataaccess;

import java.sql.Connection;
import java.sql.*;
import java.util.*;

/**
 * {@inheritDoc}
 */
public class ConnectionPoolImpl implements ConnectionPool {

    private static ConnectionPoolImpl instance = null;

    public synchronized static ConnectionPoolImpl getInstance() {
        if (instance == null) {
            instance = new ConnectionPoolImpl();
        }
        return instance;
    }

    private ConnectionPoolImpl() {
    }

    private List<Connection> availableConnections = new ArrayList<Connection>();
    private List<Connection> busyConnections = new ArrayList<Connection>();
    private String databaseUrl;
    private String user;
    private String password;
    private String schema;
    private boolean isInitialized;
    private int poolSize = 2;
    private int connectionValidTimeOut = 2000;

    /**
     * Creates the connection pool with the maximum pool size
     */
    public ConnectionPoolImpl(int poolSize) {
        this.poolSize = poolSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(String databaseUrl, String schema, String user, String password, int poolSize)
            throws DataSourceException {
        this.databaseUrl = databaseUrl;
        this.user = user;
        this.password = password;
        this.poolSize = poolSize;
        this.schema = schema;
        this.isInitialized = true;
        try {
            if (poolSize >= 1)
                availableConnections.add(createConnection());
        } catch (SQLException e) {
            throw new DataSourceException("Error occurred while initializing the pool", e);
        }
    }

    private Connection createConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(this.databaseUrl, this.user, this.password);
        connection.setSchema(this.schema);

        return connection;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized Connection getConnection() throws DataSourceException {
        Connection connection = null;
        if (!isInitialized)
            throw new DataSourceException("ConnectionPool is not initialized");

        try {
            if (availableConnections.size() == 0 && busyConnections.size() < poolSize) {
                availableConnections.add(createConnection());
            }

            if (availableConnections.size() > 0) {
                Connection availableConnection = availableConnections.get(0);
                if (availableConnection.isValid(connectionValidTimeOut)) {
                    connection = availableConnection;
                    busyConnections.add(availableConnection);
                    availableConnections.remove(connection);
                }
            }
        } catch (SQLException e) {
            throw new DataSourceException("Error occurred while getting connection", e);
        }

        return connection;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void releaseConnection(Connection connection) {
        availableConnections.add(connection);
        busyConnections.remove(connection);
    }
}
