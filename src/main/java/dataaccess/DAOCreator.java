package dataaccess;

public interface DAOCreator {

    /**
     * Sets the connection pool used by created DAOs
     * @param connectionPool
     */
    void setConnectionPool(ConnectionPool connectionPool);

    /**
     * Gets a DAO object for the given model class.
     * If DAO object for the given model class exists, that is returned.
     * Else a new one is created, stored, and returned
     * @param modelClass
     * @param <T>
     * @return
     */
    <T> DAO<T> getDAO(Class<T> modelClass) throws DataSourceException;
}
