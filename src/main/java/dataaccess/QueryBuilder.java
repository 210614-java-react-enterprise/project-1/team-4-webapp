package dataaccess;

public class QueryBuilder {


    public static String getInsert(String tableName, String columns, String values) {
        StringBuilder builder = new StringBuilder("INSERT INTO ");
        builder.append(tableName);
        builder.append("(");
        builder.append(columns);
        builder.append(")");
        builder.append(" VALUES");
        builder.append("(");
        builder.append(values);
        builder.append(")");

        return builder.toString();
    }

    public static String getUpdate(String tableName, String columns, String whereCondition) {
        StringBuilder builder = new StringBuilder("UPDATE ");
        builder.append(tableName);
        builder.append(" ");
        builder.append("SET");
        builder.append(" ");
        builder.append(columns);
        builder.append(" ");
        builder.append("WHERE ");
        builder.append(whereCondition);


        return builder.toString();
    }

    public static String getDelete(String tableName, String whereCondition) {
        StringBuilder builder = new StringBuilder("DELETE FROM ");
        builder.append(tableName);
        builder.append(" ");
        builder.append("WHERE ");
        builder.append(whereCondition);

        return builder.toString();
    }

    public static String getSelect(String tableName, String columns, String whereCondition) {
        StringBuilder builder = new StringBuilder("SELECT ");
        builder.append(columns);
        builder.append(" ");
        builder.append("FROM");
        builder.append(" ");
        builder.append(tableName);
        builder.append(" WHERE ");
        builder.append( whereCondition);

        return builder.toString();
    }

    public static String selectAll(String tableName,String columns){
        StringBuilder builder=new StringBuilder("SELECT ");
        builder.append(columns);
        builder.append(" FROM ");
        builder.append(tableName);

        return builder.toString();
    }
}
