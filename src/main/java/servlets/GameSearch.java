package servlets;

import BusinessService.BusinessException;
import BusinessService.GameServiceImpl;
import Models.Game;
import dataaccess.ConnectionPoolImpl;
import dataaccess.DAOCreator;
import dataaccess.DAOCreatorImpl;
import dataaccess.DataSourceException;
import util.HtmlHelper;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class GameSearch extends HttpServlet {

	public GameSearch(){};

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

		List<Game> games;
		String[][] kvPairs;
		String query = req.getQueryString();
		StringBuilder builder = new StringBuilder(HtmlHelper.tableHeader);
		PrintWriter html = null;

		ConnectionPoolImpl db = ConnectionPoolImpl.getInstance();
		DAOCreator daoCreator = DAOCreatorImpl.getInstance();
		daoCreator.setConnectionPool(db);
		GameServiceImpl gameService = new GameServiceImpl(daoCreator);

		try {
			db.initialize();
		} catch (DataSourceException e) {
			e.printStackTrace();
		}

		try {
			html = resp.getWriter(); //Should never fail
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (query == null) { //getQueryString returns null if there is no query string
			html.println( HtmlHelper.wrapHtml("<p>You have to actually specify a query!</p>"));
			try {
				resp.sendError(400);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error: Could not tell the client that they have misused the servlet. Message:\n" + e.getMessage());
			}

		} else { //Actual servlet logic goes into this else block
			System.out.println(query);
			kvPairs = ServletHelper.parseQueryString(query);

			System.out.println("Keys : Values");
			for (int x = 0; x < kvPairs.length; x++){
				System.out.println(kvPairs[x][0] + " : " + kvPairs[x][1]);
			}

			//TODO Ask for the ability to search on specific fields

			try {
				//db.initialize();

				games = gameService.getAllGames();

				for (Game current : games) { //Wraps the game records as HTML
					builder.append(HtmlHelper.wrapCell(current.getName()));
					//Testing html.println(HtmlHelper.wrapHtml(HtmlHelper.wrapTable(HtmlHelper.wrapRow(builder.toString()))));
				}
			} /*catch (DataSourceException e) {
				e.printStackTrace();
				System.out.println("An error occurred while connecting to the database. Message:\n" + e.getMessage());
			}*/ catch (BusinessException   e) {
				e.printStackTrace();
				System.out.println("An error occurred when fetching the games list. Message:\n" + e.getMessage());
			}

		}
	}
}
