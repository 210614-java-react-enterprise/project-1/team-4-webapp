package servlets;

import dataaccess.ConnectionPoolImpl;
//import util.Globals;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Index extends HttpServlet {

	public Index(){};

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		ConnectionPoolImpl connection = null;
		PrintWriter html = null;
		try { //Just a test
			html = resp.getWriter();
			connection = ConnectionPoolImpl.getInstance();
		} catch (IOException e) {
			e.printStackTrace();
		}

		html.println(
				"<html>" +
					"<body>" +
						"<p>" +
							"This is the Java equivalent of index.html." +
						"</p>" +
					"</body>" +
				"</html>");
	}
}
