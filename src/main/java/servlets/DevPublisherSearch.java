package servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DevPublisherSearch extends HttpServlet {

	public DevPublisherSearch(){}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

		PrintWriter html = null;
		try {
			html = resp.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}

		html.println(
				"<html>" +
					"<body>" +
						"<p>" +
							"This function should search for a dev studio or publisher and return a list of games they worked on," +
							"as well as whether the company served as a dev, publisher, or both." +
						"</p>" +
					"</body>" +
				"</html>");
	}
}
