package servlets;

import java.util.StringTokenizer;

public class ServletHelper {

	public static String[][] parseQueryString(String queryString) throws MalformedQueryStringException {
		int tokenCount;

		String[][] kvPairs;
		StringTokenizer tokenizer = new StringTokenizer(queryString,"&=");

		tokenCount = tokenizer.countTokens();

		if (tokenCount % 2 != 0) { //Rudimentary check that tokens are only key-value pairs
			throw new MalformedQueryStringException();
		}

		kvPairs = new String[tokenCount/2][2];


		tokenCount = tokenCount / 2; //Converting tokenCount to index limit

		for (int i = 0; i < tokenCount; i++) {
			kvPairs[i][0] = tokenizer.nextToken();
			kvPairs[i][1] = tokenizer.nextToken();
			System.out.println(kvPairs[i][0]+" : "+kvPairs[i][1]);

		}

		return kvPairs;
	}
}
