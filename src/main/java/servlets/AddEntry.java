package servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AddEntry extends HttpServlet {

	public AddEntry() {
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doGet(req, resp); this would just return a 405 status code

		// obtain list of objects from database or other source

		// convert objects to json (Jackson databind)

		// use the printwriter to write json to response body

		PrintWriter pw = resp.getWriter();
		pw.println("<html><body><p>This is a test!</p></body></html>");

	}
}
