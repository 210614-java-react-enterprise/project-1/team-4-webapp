package Models;

import dataaccess.annotations.*;

import java.util.Objects;

@Entity
@Table(name = "system")
public class SystemImplementation {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "system_name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public SystemImplementation(){
        super();
    }

    public SystemImplementation(String name) {
        this.name = name;
    }

    public Game getGame() {
        return game;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Systems{" +
                "game=" +
                ", studio_name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemImplementation systems = (SystemImplementation) o;
        return name.equals(systems.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash( name);
    }
}
