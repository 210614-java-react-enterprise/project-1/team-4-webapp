package Models;

import dataaccess.annotations.*;

import java.util.Objects;

@Entity
@Table(name = "genre")
public class Genre {
    @Id
    @Column(name = "genre_id")
    @GeneratedValue
    private int id;

    @Column(name = "genre_name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Genre(){
        super();
    }

    public Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Genres{" +
                "id=" + id +
                ", studio_name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return genre.id == id && (genre.name != null && genre.name.equals(name));
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
