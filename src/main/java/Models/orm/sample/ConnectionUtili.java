package orm.sample;

import BusinessService.BusinessException;
import BusinessService.GameService;
import BusinessService.GameServiceImpl;
import Models.Genre;
import Models.ReleaseDate;
import dataaccess.*;

import java.time.LocalDate;
import java.util.List;

public class ConnectionUtili {

    public static void main(String[] args) throws DataSourceException, BusinessException {

        //Initialize database connection pool

        ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
        String url = "jdbc:postgresql://gamingdb.cpqliwyfxhay.us-east-2.rds.amazonaws.com:5432/postgres";
        String password = "28282828";
        String user = "postgres";
        String schema = "public";
        connectionPool.initialize(url, schema, user, password, 2);


        //Initialize the DAOCreator
        DAOCreator creator = DAOCreatorImpl.getInstance();
        creator.setConnectionPool(connectionPool);


        //Create Business Services
        GameService gameService = new GameServiceImpl(creator);

        //Add a new Genre
        gameService.addGenre("Fantasy");
        gameService.addGenre("Zombie Apocalypse");
        gameService.addGenre("Puzzle");

        //List All genres
        List<Genre> savedGenres = gameService.getAllGenres();

        //Find Genre by name
        Genre genre = gameService.findGenreByName("Fantasy");

        //Add Release Date
        gameService.addReleaseDate(LocalDate.now(), LocalDate.now(), LocalDate.now(), "Initial release");

        //Find release date
        List<ReleaseDate> savedReleaseDates = gameService.getAllReleaseDate();
        ReleaseDate releaseDate = savedReleaseDates.get(0);

        //Save game
        gameService.addGame("fallout 4", "action role-playing game", releaseDate, genre);
    }
}
