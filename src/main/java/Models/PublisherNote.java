package Models;

import dataaccess.annotations.*;

import java.util.Objects;

@Entity
@Table(name = "publisher_notes")
public class PublisherNote {

    @Id
    @GeneratedValue
    @Column
    private int id;

    @Column(name = "publisher_notes")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PublisherNote(){
        super();
    }

    public PublisherNote( String name) {
        this.id=id;
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PublisherNote{" +
                "game=" +
                ", studio_name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PublisherNote publisherNotes = (PublisherNote) o;
        return  name.equals(publisherNotes.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash( name);
    }
}
