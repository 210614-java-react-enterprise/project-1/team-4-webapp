package BusinessService;

import Models.*;
import dataaccess.DAO;
import dataaccess.DAOCreator;
import dataaccess.DAOException;
import dataaccess.DataSourceException;

import java.util.List;

/**
 * {@inheritDoc}
 */
public class PublisherServiceImpl implements PublisherService {

    private final DAOCreator daoCreator;

    public PublisherServiceImpl(DAOCreator daoCreator) {
        this.daoCreator = daoCreator;
    }

    @Override
    public void addPublisher(String name, PublisherNote publisherNote, Game game) throws BusinessException {
        try {
            DAO<Publisher> publisherDAO = daoCreator.getDAO(Publisher.class);
            Publisher publisher = new Publisher(name, publisherNote, game);
            publisherDAO.create(publisher);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public void addPublisherNote(String name) throws BusinessException {
        try {
            DAO<PublisherNote> publisherNoteDAO = daoCreator.getDAO(PublisherNote.class);
            PublisherNote publisherNote = new PublisherNote(name);
            publisherNoteDAO.create(publisherNote);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public List<Publisher> getAllPublishers() throws BusinessException {
        List<Publisher> publishers = null;

        try {
            DAO<Publisher> publisherDAO = daoCreator.getDAO(Publisher.class);
            publishers = publisherDAO.findAll();
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return publishers;
    }

    @Override
    public Publisher findPublisher(int id) throws BusinessException {
        Publisher publisher = null;
        try {
            DAO<Publisher> PublisherDAO = daoCreator.getDAO(Publisher.class);
            publisher = PublisherDAO.find(id);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return publisher;
    }

    @Override
    public void removePublisher(int id) throws BusinessException {
        try {
            DAO<Publisher> publisherDAO = daoCreator.getDAO(Publisher.class);
            Publisher publisher = publisherDAO.find(id);
            publisherDAO.delete(publisher);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public Publisher updatePublisher(int id) throws BusinessException {
        Publisher publisher = null;
        try {
            DAO<Publisher> publisherDAO = daoCreator.getDAO(Publisher.class);
            publisher = publisherDAO.find(id);
            publisherDAO.update(publisher);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return publisher;
    }
}

