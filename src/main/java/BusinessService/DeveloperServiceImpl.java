package BusinessService;

import Models.*;
import dataaccess.DAO;
import dataaccess.DAOCreator;
import dataaccess.DAOException;
import dataaccess.DataSourceException;

import java.util.List;

/**
 * {@inheritDoc}
 */
public class DeveloperServiceImpl implements DeveloperService{
    private final DAOCreator daoCreator;

    public DeveloperServiceImpl(DAOCreator daoCreator) {

        this.daoCreator = daoCreator;
    }

    @Override
    public void addDeveloper(Game game, String studio_name, SystemImplementation systemImplementation) throws BusinessException {

        try {
            DAO<Developer> developerDAO = daoCreator.getDAO(Developer.class);
            Developer developer = new Developer(game,studio_name,systemImplementation);
            developerDAO.create(developer);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public void addSystem(String name) throws BusinessException {
        try {
            DAO<SystemImplementation> systemImplementationDAO = daoCreator.getDAO(SystemImplementation.class);
            SystemImplementation systemImplementation = new SystemImplementation(name);
            systemImplementationDAO.create(systemImplementation);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public List<Developer> getAllDeveloper(Game game, String studio_name, SystemImplementation systemImplementation) throws BusinessException {
        List<Developer> developers = null;

        try {
            DAO<Developer> developerDAO = daoCreator.getDAO(Developer.class);
            developers = developerDAO.findAll();
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return developers;
    }

    @Override
    public Developer findDeveloper(int id) throws BusinessException {
        Developer developer = null;
        try {
            DAO<Developer> developerDAO = daoCreator.getDAO(Developer.class);
            developer = developerDAO.find(id);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return developer;
    }

    @Override
    public void removeDeveloper(int id) throws BusinessException {

        try {
            DAO<Developer> developerDAO = daoCreator.getDAO(Developer.class);
            Developer developer = developerDAO.find(id);
            developerDAO.delete(developer);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
    }

    @Override
    public Developer updateDeveloper(int id) throws BusinessException {
        Developer developer = null;
        try {
            DAO<Developer> developerDAO = daoCreator.getDAO(Developer.class);
            developer = developerDAO.find(id);
            developerDAO.update(developer);
        } catch (DataSourceException | DAOException e) {
            throw new BusinessException(e);
        }
        return developer;
    }
}
