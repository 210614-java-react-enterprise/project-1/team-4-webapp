package BusinessService;

import Models.*;
import java.util.*;

public interface DeveloperService {

    void addDeveloper(Game game, String studio_name, SystemImplementation systemImplementation) throws BusinessException;

    void addSystem(String name) throws BusinessException;

    List<Developer> getAllDeveloper(Game game, String studio_name, SystemImplementation systemImplementation) throws BusinessException;

    Developer findDeveloper(int id) throws BusinessException;

    void removeDeveloper(int id) throws BusinessException;

    Developer updateDeveloper(int id) throws BusinessException;


}
