package Models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PublisherNoteTest {
    private static PublisherNote publisherNote = new PublisherNote();
    private static Game game = new Game();

    @Test
    void getName() {
        game = new Game(5, "Valorant", "Description");
        publisherNote = new PublisherNote( "Action", game);
        assertEquals("Action", publisherNote.getName());
    }

    @Test
    void setName() {
        game = new Game(5, "Valorant", "Description");
        publisherNote.setName("FPS", game);
        assertEquals("FPS", publisherNote.getName());
    }

    @Test
    void getGame() {
        game = new Game(5, "Valorant", "Description");
        publisherNote = new PublisherNote( "Action", game);
        assertEquals(game, publisherNote.getGame());
    }

    @Test
    void setGame() {
        game = new Game(2, "Apex", "adadadad");
        publisherNote.setGame(game);
        assertEquals(game, publisherNote.getGame());
    }
}