package Models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    private static Game game = new Game();

    @Test
    void getId() {
        game = new Game(5, "Valorant", "Description");
        assertEquals(5, game.getId());
    }

    @Test
    void setId() {
        game.setId(4);
        assertEquals(4, game.getId());
    }

    @Test
    void getName() {
        game = new Game(5, "Valorant", "Description");
        assertEquals("Valorant", game.getName());
    }

    @Test
    void setName() {
        game.setName("Apex");
        assertEquals("Apex", game.getName());
    }

    @Test
    void getDescription() {
        game = new Game(5, "Valorant", "Description");
        assertEquals("Descirption", game.getName());
    }

    @Test
    void setDescription() {
        game.setDescription("Apex");
        assertEquals("Apex", game.getDescription());
    }
}