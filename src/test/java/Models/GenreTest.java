package Models;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GenreTest {
    private static Genre genre = new Genre();
    private static Game game = new Game();

    @Test
    void getName() {
        game = new Game(5, "Valorant", "Description");
        genre = new Genre( "Action", game);
        assertEquals("Action", genre.getName());
    }

    @Test
    void setName() {
        genre.setName("FPS");
        assertEquals("FPS", genre.getName());
    }

    @Test
    void getGame() {
        game = new Game(5, "Valorant", "Description");
        genre = new Genre( "Action", game);
        assertEquals(game, genre.getGame());
    }

    @Test
    void setGame() {
        game = new Game(2, "Apex", "adadadad");
        genre.setGame(game);
        assertEquals(game, genre.getGame());
    }
}