package dataaccess;

import Models.Publisher;
import dataaccess.annotations.Column;
import dataaccess.annotations.Id;
import dataaccess.annotations.Table;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DAOImplTest {

    @Table
    private static class TestEntity1 {
    }

    private static class TestEntity2 {
    }

    @Table
    public static class TestEntity3 {
        @Id
        @Column
        private int id;
    }

    @Mock
    Connection connection;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    ResultSet results;

    @BeforeEach
    void initializeTest() throws DAOException, SQLException {
        MockitoAnnotations.openMocks(this);


    }

    @Test
    @DisplayName("Given a class annotated with Table, create should throw no exceptions")
    void createForTableAnnotatedClass() throws DataSourceException, SQLException {
        //Arrange
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        ConnectionPool connectionPool = mock(ConnectionPool.class);
        when(connectionPool.getConnection()).thenReturn(connection);
        DAOImpl<TestEntity1> dao = new DAOImpl<TestEntity1>(connectionPool, TestEntity1.class);

        //Act
        Executable create = () -> dao.create(new TestEntity1());

        //Assert
        assertDoesNotThrow(create);
    }

    @Test
    @DisplayName("Given a class NOT annotated with Table, create should throw IllegalStateException")
    void createForNonAnnotatedClass() throws DataSourceException {
        //Arrange
        ConnectionPool connectionPool = mock(ConnectionPool.class);
        when(connectionPool.getConnection()).thenReturn(connection);
        DAOImpl<TestEntity2> dao = new DAOImpl<TestEntity2>(connectionPool, TestEntity2.class);

        //Act
        Executable create = () -> dao.create(new TestEntity2());

        //Assert
        assertThrows(IllegalStateException.class, create);
    }

    @Test
    @DisplayName("Given a class annotated with Table, update should throw no exceptions")
    void update() throws SQLException, DataSourceException {
        //Arrange
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        ConnectionPool connectionPool = mock(ConnectionPool.class);
        when(connectionPool.getConnection()).thenReturn(connection);
        DAOImpl<TestEntity1> dao = new DAOImpl<TestEntity1>(connectionPool, TestEntity1.class);

        //Act
        Executable update = () -> dao.update(new TestEntity1());

        //Assert
        assertDoesNotThrow(update);
    }

    @Test
    @DisplayName("Given a class annotated with Table, delete should throw no exceptions")
    void delete() throws SQLException, DataSourceException {
        //Arrange
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        ConnectionPool connectionPool = mock(ConnectionPool.class);
        when(connectionPool.getConnection()).thenReturn(connection);
        DAOImpl<TestEntity1> dao = new DAOImpl<TestEntity1>(connectionPool, TestEntity1.class);

        //Act
        Executable delete = () -> dao.delete(new TestEntity1());

        //Assert
        assertDoesNotThrow(delete);
    }

    @Test
    @DisplayName("Given a primary key, find returns the expected entity")
    void find() throws SQLException, DataSourceException, DAOException, NoSuchMethodException {
        //Arrange
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        ConnectionPool connectionPool = mock(ConnectionPool.class);
        when(connectionPool.getConnection()).thenReturn(connection);
        when(preparedStatement.executeQuery()).thenReturn(results);
        when(results.getObject(any(int.class))).thenReturn(1);
        when(results.next()).thenReturn(true, true, true, false);

        DAOImpl<TestEntity3> dao = new DAOImpl<TestEntity3>(connectionPool, TestEntity3.class);

        //Act
        TestEntity3 entity  = dao.find(1);

        //Assert
        assertEquals(entity.id, 1);
    }

    @Test
    @DisplayName("findAll returns all entities")
    void findAll() throws SQLException, DataSourceException, DAOException {
        //Arrange
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        ConnectionPool connectionPool = mock(ConnectionPool.class);
        when(connectionPool.getConnection()).thenReturn(connection);
        when(preparedStatement.executeQuery()).thenReturn(results);
        when(results.getObject(any(int.class))).thenReturn(1);
        when(results.next()).thenReturn(true, true, true, false);

        DAOImpl<TestEntity3> dao = new DAOImpl<TestEntity3>(connectionPool, TestEntity3.class);

        //Act
        List<TestEntity3> returnedEntities  = dao.findAll();

        //Assert
        assertEquals(returnedEntities.size(), 3);
    }
//
    @Test
    @DisplayName("Given a map of columns and their values, findWhere returns all matching entities")
    void findWhere() throws SQLException, DataSourceException, DAOException {
        //Arrange
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        ConnectionPool connectionPool = mock(ConnectionPool.class);
        when(connectionPool.getConnection()).thenReturn(connection);
        when(preparedStatement.executeQuery()).thenReturn(results);
        when(results.getObject(any(int.class))).thenReturn(1);
        when(results.next()).thenReturn(true, false);

        DAOImpl<TestEntity3> dao = new DAOImpl<TestEntity3>(connectionPool, TestEntity3.class);

        //Act
        List<TestEntity3> returnedEntities  = dao.findWhere(Collections.singletonMap("id",1));

        //Assert
        assertEquals(returnedEntities.size(), 1);
    }
}