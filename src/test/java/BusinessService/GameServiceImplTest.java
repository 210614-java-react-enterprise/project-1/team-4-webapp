package BusinessService;

import Models.*;
import dataaccess.DAO;
import dataaccess.DAOCreator;
import dataaccess.DAOException;
import dataaccess.DataSourceException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GameServiceImplTest {

    @Mock
    DAOCreator daoCreator;

    @BeforeEach
    void initializeTest() {
        openMocks(this);
    }

    @Test
    @DisplayName("Add Genre should throw no exceptions")
    void addGenre() throws DataSourceException {
        //Arrange
        DAO<Genre> mockGenreDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Genre.class)).thenReturn(mockGenreDAO);
        GameServiceImpl gameService = new GameServiceImpl(daoCreator);

        //Act
        Executable addGenre = () -> gameService.addGenre("Test Genre name");

        //Assert
        assertDoesNotThrow(addGenre);
    }

    @Test
    @DisplayName("Add ReleaseDate should throw no exceptions")
    void addReleaseDate() throws DataSourceException{
        //Arrange
        DAO<ReleaseDate> mockReleaseDateDAO =  mock(DAO.class);
        when(daoCreator.getDAO(ReleaseDate.class)).thenReturn(mockReleaseDateDAO);
        GameServiceImpl gameService = new GameServiceImpl(daoCreator);

        //Act
        Executable addReleaseDate = () -> gameService.addReleaseDate(null,null,null,null);

        //Assert
        assertDoesNotThrow(addReleaseDate);
    }

    @Test
    @DisplayName("Add Game should throw no exceptions")
    void addGame() throws DataSourceException {
        //Arrange
        DAO<Game> mockGameDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Game.class)).thenReturn(mockGameDAO);
        GameServiceImpl gameService = new GameServiceImpl(daoCreator);

        //Act
        Executable addGame = () -> gameService.addGame("TestGame",null,null,null);

        //Assert
        assertDoesNotThrow(addGame);
    }

    @Test
    void getAllGenres() throws BusinessException, DataSourceException, DAOException {
        //Arrange
        List<Genre> expectedGenres = Arrays.asList(new Genre(), new Genre());
        DAO<Genre> mockGenreDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Genre.class)).thenReturn(mockGenreDAO);
        when(mockGenreDAO.findAll()).thenReturn(expectedGenres);
        GameServiceImpl gameService = new GameServiceImpl(daoCreator);

        //Act
        List<Genre> returnedGenres = gameService.getAllGenres();

        //Assert
        assertIterableEquals(expectedGenres, returnedGenres);
    }

    @Test
    void getAllGames() throws BusinessException, DataSourceException, DAOException {
        //Arrange
        List<Game> expectedGames = Arrays.asList(new Game(), new Game());
        DAO<Game> mockGameDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Game.class)).thenReturn(mockGameDAO);
        when(mockGameDAO.findAll()).thenReturn(expectedGames);
        GameServiceImpl gameService = new GameServiceImpl(daoCreator);

        //Act
        List<Game> returnedGames = gameService.getAllGames();

        //Assert
        assertIterableEquals(expectedGames, returnedGames);
    }

    @Test
    void findGame() throws BusinessException, DAOException, DataSourceException {
        //Arrange
        DAO<Game> mockGameDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Game.class)).thenReturn(mockGameDAO);
        Game game = new Game();
        game.setId(1);
        when(mockGameDAO.find(1)).thenReturn(game);
        GameServiceImpl gameService = new GameServiceImpl(daoCreator);

        //Act
        Game returnedGame = gameService.findGame(1);

        //Assert
        assertEquals(1, returnedGame.getId());
    }

    @Test
    @DisplayName("Remove game should throw no exceptions")
    void removeGame() throws DataSourceException {
        //Arrange
        DAO<Game> mockGameDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Game.class)).thenReturn(mockGameDAO);
        GameServiceImpl gameService = new GameServiceImpl(daoCreator);

        //Act
        Executable removeGame = () -> gameService.removeGame(1);

        //Assert
        assertDoesNotThrow(removeGame);
    }

    @Test
    @DisplayName("Update game should throw no exceptions")
    void updateGame() throws DataSourceException {
        //Arrange
        DAO<Game> mockGameDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Game.class)).thenReturn(mockGameDAO);
        GameServiceImpl gameService = new GameServiceImpl(daoCreator);

        //Act
        Executable upDateGame = () -> gameService.updateGame(1);

        //Assert
        assertDoesNotThrow(upDateGame);
    }
}