package BusinessService;

import Models.Game;
import Models.Publisher;
import Models.PublisherNote;
import dataaccess.DAO;
import dataaccess.DAOCreator;
import dataaccess.DAOException;
import dataaccess.DataSourceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import org.junit.jupiter.api.function.Executable;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PublisherServiceImplTest {

    @Mock
    DAOCreator daoCreator;

    @BeforeEach
    void initializeTest() {
        openMocks(this);
    }

    @Test
    @DisplayName("Add publisher should throw no exceptions")
    void addPublisher() throws DataSourceException {
        //Arrange
        DAO<Publisher> mockPublisherDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Publisher.class)).thenReturn(mockPublisherDAO);
        PublisherServiceImpl publisherService = new PublisherServiceImpl(daoCreator);

        //Act
        Executable addPublisher = () -> publisherService.addPublisher("TestPublisher",null, null);

        //Assert
        assertDoesNotThrow(addPublisher);
    }

    @Test
    @DisplayName("Add publisher note should throw no exceptions")
    void addPublisherNote() throws DataSourceException {
        //Arrange
        DAO<PublisherNote> mockPublisherNoteDAO =  mock(DAO.class);
        when(daoCreator.getDAO(PublisherNote.class)).thenReturn(mockPublisherNoteDAO);
        PublisherServiceImpl publisherService = new PublisherServiceImpl(daoCreator);

        //Act
        Executable addPublisherNote = () -> publisherService.addPublisherNote("Test Publisher Note");

        //Assert
        assertDoesNotThrow(addPublisherNote);
    }

    @Test
    void getAllPublishers() throws DataSourceException, DAOException, BusinessException {
        //Arrange
        List<Publisher> expectedPublishers = Arrays.asList(new Publisher(), new Publisher());
        DAO<Publisher> mockPublisherDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Publisher.class)).thenReturn(mockPublisherDAO);
        when(mockPublisherDAO.findAll()).thenReturn(expectedPublishers);
        PublisherServiceImpl publisherService = new PublisherServiceImpl(daoCreator);

        //Act
        List<Publisher> returnedPublishers = publisherService.getAllPublishers();

        //Assert
        assertIterableEquals(expectedPublishers, returnedPublishers);
    }

    @Test
    void findPublisher() throws DataSourceException, DAOException, BusinessException {
        //Arrange
        DAO<Publisher> mockPublisherDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Publisher.class)).thenReturn(mockPublisherDAO);
        Publisher publisher = new Publisher();
        publisher.setId(1);
        when(mockPublisherDAO.find(1)).thenReturn(publisher);
        PublisherServiceImpl publisherService = new PublisherServiceImpl(daoCreator);

        //Act
        Publisher returnedPublisher = publisherService.findPublisher(1);

        //Assert
        assertEquals(1, returnedPublisher.getId());
    }

    @Test
    @DisplayName("Remove publisher should throw no exceptions")
    void removePublisher() throws DataSourceException {
        //Arrange
        DAO<Publisher> mockPublisherDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Publisher.class)).thenReturn(mockPublisherDAO);
        PublisherServiceImpl publisherService = new PublisherServiceImpl(daoCreator);

        //Act
        Executable removePublisher = () -> publisherService.removePublisher(1);

        //Assert
        assertDoesNotThrow(removePublisher);
    }

    @Test
    @DisplayName("Update publisher should throw no exceptions")
    void updatePublisher() throws DataSourceException {
        //Arrange
        DAO<Publisher> mockPublisherDAO =  mock(DAO.class);
        when(daoCreator.getDAO(Publisher.class)).thenReturn(mockPublisherDAO);
        PublisherServiceImpl publisherService = new PublisherServiceImpl(daoCreator);

        //Act
        Executable upDatePublisher = () -> publisherService.updatePublisher(1);

        //Assert
        assertDoesNotThrow(upDatePublisher);
    }
}
